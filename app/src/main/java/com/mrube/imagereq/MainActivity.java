package com.mrube.imagereq;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

public class MainActivity extends AppCompatActivity {

    ImageView iv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iv = (ImageView) findViewById(R.id.iv);


        ImageLoader imageLoader = AppController.getInstance().getImageLoader();

// If you are using normal ImageView
        // i added a simple image url here, u can change here and get yours
        imageLoader.get("https://scontent-sit4-1.xx.fbcdn.net/t31.0-8/13316879_1159809004043188_5417204189873716643_o.jpg", new ImageLoader.ImageListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("My Tag", "Image Load Error: " + error.getMessage());
            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean arg1) {
                if (response.getBitmap() != null) {
                    // load image into imageview
                    iv.setImageBitmap(response.getBitmap());
                }
            }
        });

    }


    public void uploadImage(View v){
        Intent i = new Intent(MainActivity.this, UploadImg.class);
        startActivity(i);
    }

}
